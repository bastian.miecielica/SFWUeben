package MSE.SWF.exam.TransportMessage;

import MSE.SWF.exam.Message.Message;
import MSE.SWF.exam.User.User;

/**
 * Created by basti on 17.04.2016.
 */
public interface TransportMessage {
    public String getAdress();
    public boolean sendMessage(Message Message);
    /**
     * If the user sends a message the status is Beschäftigt(=1)
     * @param user
     * @return
     */
    public boolean setStatus(User user);

}
