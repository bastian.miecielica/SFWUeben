package MSE.SWF.exam.User;


/**
 * Created by basti on 17.04.2016.
 */
public interface User {
    public String GetName();
    public String getAdress();
    /**
     * 0=Offline, 1=Beschäftigt, 2=Offline
     * @return
     */
    public String getStatus();

}
