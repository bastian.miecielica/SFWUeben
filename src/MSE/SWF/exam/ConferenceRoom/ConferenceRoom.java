package MSE.SWF.exam.ConferenceRoom;

import MSE.SWF.exam.User.User;

/**
 * Created by basti on 17.04.2016.
 */
public interface ConferenceRoom {
    public boolean enterConferenceRoom(User user);
    public boolean leaveConferenceRoom(User user);

    /**
     * if user enters Conferenceroom set the Status is Online(=1), if  the user leaves the status is offline(=0)
     * @param user
     * @return
     */
    public boolean setStatus(User user);
}
