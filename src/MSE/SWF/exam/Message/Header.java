package MSE.SWF.exam.Message;

import MSE.SWF.exam.User.User;

/**
 * Created by basti on 17.04.2016.
 */
public interface Header {
    public User getUser();
    public String getSubject();

}
