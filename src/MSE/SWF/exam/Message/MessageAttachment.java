package MSE.SWF.exam.Message;

/**
 * Created by basti on 17.04.2016.
 */
public interface MessageAttachment<T> {
    public T getAttachment();
}
