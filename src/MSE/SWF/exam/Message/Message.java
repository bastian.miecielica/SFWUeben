package MSE.SWF.exam.Message;

/**
 * Created by basti on 17.04.2016.
 */
public interface Message {
    public Header GetHeader();
    public String GetBody();
    public MessageAttachment getAttachment();

}
