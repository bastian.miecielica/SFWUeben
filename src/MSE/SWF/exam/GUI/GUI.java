package MSE.SWF.exam.GUI;

import MSE.SWF.exam.Message.Message;
import MSE.SWF.exam.User.User;

import java.util.List;

/**
 * Created by basti on 17.04.2016.
 */
public interface GUI {
    public boolean enterMessage();
    public Message getMessage();
    public void showMessage();
    public boolean setStatus(User user);
    public List<User> listUser();
}
