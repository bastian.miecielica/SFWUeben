﻿MSE/SWF Prüfungsvorlage
=======================

JAva Template für die Prüfung. Damit die Prüfung erfolgreich abgegeben werden kann müssen folgende Kriterien erfüllt sein:

* Benutzen Sie bitte die Vorlage. Sie ist so vorbereitet, dass sie am Jenkins verwendet werden kann.
* Richten Sie ein Repository ein und geben Sie durch hochladen (git push) ab.

Diese Vorlage wurde für Eclipse erstellt. Sie können aber jede andere Entwicklungsumgebung benutzen, solange build.xml am Jenkins kompilierbar bleibt.

> Bereiten Sie bitte alles **VOR** der Prüfung vor!

Repository
----------
https://inf-swe-git.technikum-wien.at/

Das Repository ist selbst anzulegen: 

* my dashboard 
* owned 
* new repository 
* se00x000/MSE-SS??-SWF-Exam

Das Repository hat die URL: https://se00x000@inf-swe-git.technikum-wien.at/?r=~se00x000/MSE-SS??-SWF-Exam.git

* se00x000 ist durch Ihre se-Nummer zu ersetzen
* MSE-SS??-SWF-Exam durch das Jahr (SS 15 -> MSE-SS15-SWF-Exam)

Setup des Projektes
-------------------
https://inf-swe-git.technikum-wien.at/summary/?r=MSE/SWF-Exam.git

Clonen Sie das Template in ein Verzeichnis Ihrer Wahl und ändern Sie anschließend den remote/origin auf Ihr Repository
	
	git clone https://inf-swe-git.technikum-wien.at/r/MSE/SWF-Exam.git
	cd SWF-Exam
	git remote set-url origin https://se00x000@inf-swe-git.technikum-wien.at/r/~se00x000/MSE-SS??-SWF-Exam.git
    git push origin master

Überprüfen Sie im Anschluss, ob Jenkins Ihre erste "Abgabe" erkannt hat und fehlerfrei kompilieren konnte.

Prüfung
-------
Alle weitere Angaben zur Prüfung finden Sie in einem gesonderten Dokument.
